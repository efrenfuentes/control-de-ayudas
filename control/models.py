from __future__ import unicode_literals

from django.db import models


class Municipio(models.Model):
    nombre = models.CharField(max_length=50, blank=False, unique=True)

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return u"{}".format(self.nombre)

    class Meta:
        ordering = ['nombre']


class TipoAyuda(models.Model):
    nombre = models.CharField(max_length=50, blank=False, unique=True)
    descripcion = models.TextField(blank=False, default="")

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return u"{}".format(self.nombre)

    class Meta:
        ordering = ['nombre']


class Beneficiario(models.Model):
    cedula = models.CharField(max_length=10, blank=False, unique=True)
    nombres = models.CharField(max_length=50, blank=False)
    apellidos = models.CharField(max_length=50, blank=False)
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    direccion = models.TextField(blank=False, default="")
    telefono = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField()
    ayudas = models.ManyToManyField(TipoAyuda, through='Ayuda')

    def nombre_completo(self):
        return "{}, {}".format(self.apellidos, self.nombres)

    def __str__(self):
        return "{} - {}".format(self.cedula, self.nombre_completo())

    def __unicode__(self):
        return u"{} - {}".format(self.cedula, self.nombre_completo())

    class Meta:
        ordering = ['cedula']


class Ayuda(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE)
    tipo_ayuda = models.ForeignKey(TipoAyuda, on_delete=models.CASCADE)
    fecha = models.DateField(blank=False, auto_now=False)
    notas = models.TextField(blank=False, default="")

    class Meta:
        ordering = ['-fecha']

