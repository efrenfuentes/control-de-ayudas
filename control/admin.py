from django.contrib import admin
from .models import Beneficiario, Municipio, TipoAyuda, Ayuda


@admin.register(Municipio)
class MunucipioAdmin(admin.ModelAdmin):
    fields = ['nombre']
    list_display = ['nombre']
    search_fields = ['nombre']


@admin.register(TipoAyuda)
class TipoAyudaAdmin(admin.ModelAdmin):
    fields = ['nombre', 'descripcion']
    list_display = ['nombre']
    search_fields = ['nombre']


class AyudaInline(admin.TabularInline):
    model = Ayuda

@admin.register(Beneficiario)
class Beneficiario(admin.ModelAdmin):
    list_display = ['cedula', 'nombres', 'apellidos']
    search_fields = ['cedula', 'nombres', 'apellidos']
    list_filter = ['municipio', 'ayudas']
    fieldsets = (
        ('Datos Personales', {
            'fields': ('cedula', 'apellidos', 'nombres', 'fecha_nacimiento')
        }),
        ('Datos de Direccion', {
            'fields': ('municipio', 'direccion', 'telefono'),
        }),
    )
    inlines = [
        AyudaInline,
    ]

